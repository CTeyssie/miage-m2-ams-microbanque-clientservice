package fr.miage.toulouse.m2.ams.banqueclientservice.repo;

import fr.miage.toulouse.m2.ams.banqueclientservice.entities.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository de gestion des clients de la banque
 */
@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {
}
