package fr.miage.toulouse.m2.ams.banqueclientservice.expo;

import fr.miage.toulouse.m2.ams.banqueclientservice.entities.Client;
import fr.miage.toulouse.m2.ams.banqueclientservice.metier.Metier;
import fr.miage.toulouse.m2.ams.banqueclientservice.utilities.ClientDupliqueException;
import fr.miage.toulouse.m2.ams.banqueclientservice.utilities.ClientInconnuException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Service d'exposition REST des clients.
 * URL / exposée.
 */
@RestController
/*
    AVANT : @RequestMapping("/api/clients")
    Modif : plus besoin de /api/clients via
    l'usage de l'API Gateway qui va masquer le chemin de l'URL
 */
@RequestMapping("/")
public class ClientController {
    Logger logger = LoggerFactory.getLogger(ClientController.class);

    // Injection DAO clients
    private final Metier metier;

    public ClientController(Metier metier) {
        this.metier = metier;
    }

    /**
     * GET 1 client
     * @param id id du client
     * @return Client converti en JSON
     */
    @GetMapping("{id}")
    public Client getClient(@PathVariable("id") Long id) throws ClientInconnuException {
        logger.info("Client : demande récup d'un client avec id:{}", id);
        return metier.rechercherClient(id);
    }

    /**
     * GET liste des clients
     * @return liste des clients en JSON. [] si aucun compte.
     */
    @GetMapping("")
    public List<Client> getClients() {
        logger.info("Client : demande récup des comptes clients");
        return metier.listerClients();
    }

    /**
     * POST un client
     * @param client client à ajouter (import JSON)
     * @return client ajouté
     */
    @PostMapping("")
    public Client postClient(@RequestBody Client client) throws ClientDupliqueException {
        logger.info("Client : demande CREATION d'un client avec id:{}", client.getId());
        return metier.creerClient(client);
    }

    /**
     * PUT un client
     * @param id id du client à modifier
     * @param client client modifié (import JSON)
     * @return client modifié
     */
    @PutMapping("{id}")
    public Client putClient(@PathVariable("id") Long id, @RequestBody Client client) throws ClientInconnuException {
        logger.info("Client : demande MODIFICATION d'un client avec id:{}", id);
        return metier.modifierClient(id, client);
    }

    /**
     * DELETE un client
     * @param id id du client à supprimer
     */
    @DeleteMapping("{id}")
    public void deleteClient(@PathVariable("id") Long id) {
        logger.info("Client : demande SUPPRESSION d'un client avec id:{}", id);
        metier.supprimerClient(id);
    }

}
