package fr.miage.toulouse.m2.ams.banqueclientservice.utilities;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ClientInconnuException extends Exception {
}
