package fr.miage.toulouse.m2.ams.banqueclientservice.metier;

import fr.miage.toulouse.m2.ams.banqueclientservice.entities.Client;
import fr.miage.toulouse.m2.ams.banqueclientservice.repo.ClientRepository;
import fr.miage.toulouse.m2.ams.banqueclientservice.utilities.ClientDupliqueException;
import fr.miage.toulouse.m2.ams.banqueclientservice.utilities.ClientInconnuException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Metier {

    private final ClientRepository clientRepository;

    public Metier(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public List<Client> listerClients() {
        return (List<Client>) clientRepository.findAll();
    }

    public Client creerClient(Client client) throws ClientDupliqueException {
        if (clientRepository.existsById(client.getId())) throw new ClientDupliqueException();
        return clientRepository.save(client);
    }

    public Client rechercherClient(Long id) throws ClientInconnuException {
        return clientRepository.findById(id).orElseThrow(ClientInconnuException::new);
    }

    public void supprimerClient(Long id) {
        clientRepository.deleteById(id);
    }

    public Client modifierClient(Long id, Client client) throws ClientInconnuException {
        if (!clientRepository.existsById(id)) throw new ClientInconnuException();
        client.setId(id);
        return clientRepository.save(client);
    }


}
